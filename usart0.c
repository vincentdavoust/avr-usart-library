#include "usart0.h"
#include <avr/io.h>

void uart0_init(unsigned int ubrr){
	DDRE=(1<<PE1);

	UBRR0H = (unsigned char)(ubrr>>8) ; // UBRRH = 0x00
	UBRR0L = (unsigned char)ubrr ; // UBRRL = 11 : UBRR = f(osc)/(16*BAUD)

	UCSR0C = (3<<UCSZ00);								// USBS : bit STOP
														// UCSZ0 : nb de bit transmis
														// Aucune Parite: UPM1=0 et UPM = 0
	UCSR0B |= (1 << TXEN0); /*(1 << RXEN0) |*/



}

int uart0_putchar(char car, FILE *stream)// fonction envoyer caractere
{
	while(!(UCSR0A&(1<<UDRE0)));// attendre fin d'emission du caractere precedent

	UDR0 = car;//envoyer caractere

	return 0;
}




unsigned char uart0_getchar(void) {// fonction attendre reception
	while(!(UCSR0A&(1<<RXC0))); // attendre la reception
	
	return UDR0;
}
