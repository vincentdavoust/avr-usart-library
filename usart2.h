/*
** usart2.h for project in /home/vincent/c/myFAT/USART
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Thu Feb  6 20:32:40 2014 Vincent
** Last update Thu Feb  6 20:34:28 2014 Vincent
** 
** Prefix : 
*/

#ifndef USART2_H_
# define USART2_H_

#define KBAUD 120

void		usart2_spi_init(unsigned int kbaud);
unsigned char	usart2_spi_data(char data);
void		usart2_spi_data_fast(char data);

#endif /* !USART2_H_ */
