#ifndef USART0_H_
#define USART0_H_

#include <stdio.h>

void uart0_init(unsigned int ubrr);
int uart0_putchar(char car, FILE *stream);
unsigned char uart0_getchar(void);


#endif /* !USART0_H_ */

