#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include "usart2.h"

//#define F_CPU 
/*
output n'etait pas defini , warning! Nappe bus pirate utilis� --> mega diaphonie
!!!!! ne pas laisser le @!�?/-0! de ISP de programmation --> peut provoquer un reset de l'Atmega 1280 
*/


void usart2_spi_init(unsigned int kbaud){

  UBRR2 = 0;

  DDRH |= (1<<PH1)|(1<<PH2)|(1<<PH3);;// txd2, clock, definit comme sortie /// 
  /* Definir les parametres uart et l'initialiser le mode spi*/
  UCSR2C = (1<<UMSEL21)|(1<<UMSEL20);// UMSEL21=UMSEL20=1 --> Mode SPI activer

  /* activer reception et transmission */
  UCSR2B = (1<<RXEN2)|(1<<TXEN2);
  /* Set baud rate. */

  UBRR2 = (F_CPU / (2 * kbaud)) / 1000  - 1;
  /* doc ATmega1280 : IMPORTANT: The Baud Rate must be set after the transmitter is enabled*/
}


unsigned char usart2_spi_data(char data) {
  /* on attends que le buffer de transmision se vide */
  while ( !( UCSR2A & (1<<UDRE2)) );

  /* on met data dans le buffer et on l'envoie */
  UDR2 = data;// data peut seulement etre ecrit dans UDR2 si flag UDRE2=1 

  /* attendre que l'on recoit les donn�es attendus */
  while ( !(UCSR2A & (1<<RXC2)) );
  /* revoyer les donn�es recues */
  return UDR2;
}

void usart2_spi_data_fast(char data) {
  /* on met data dans le buffer et on l'envoie */
  UDR2 = data;// data peut seulement etre ecrit dans UDR2 si flag UDRE2=1 
}
