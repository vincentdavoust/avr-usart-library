/*
** usart1.h for project in /home/vincent/c/datalogger_FAT
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Wed Mar 12 17:26:37 2014 Vincent
** Last update Wed Mar 12 17:32:56 2014 Vincent
*/

#ifndef USART1_H_
# define USART1_H_

#include <avr/io.h>
#include <stdio.h>

void	uart1_init(int ubrr);
void	uart1_switch_mode_int(void);
int	uart1_putchar(char c, FILE *stream);	//fonction d'envoi de caractere (prototype imposee par stdio.h)
void	uart1_send_char_fast(char c);
unsigned char uart1_getchar(void);


#endif /* !USART1_H_ */
