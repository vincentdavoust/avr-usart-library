/*
** usart1.c for project in /home/vincent/c/datalogger_FAT
** 
** Made by Vincent
** Mail <vincent.davoust@gmail.com>
** 
** Started on  Wed Mar 12 17:26:42 2014 Vincent
** Last update Wed Mar 12 17:28:00 2014 Vincent
*/

#include "usart1.h"

/**************		Initialisation de l'UART 1		**************/

void uart1_init(int ubrr) // initialisation de luart
{
	DDRD |= (1<<PD3); // Port PD3 en sortie

	// UBRR regle la vitesse d'emission
	UBRR1H = (unsigned char)(ubrr>>8) ; // UBRRH = 0x00
	UBRR1L = (unsigned char)ubrr ; // UBRRL = 11 : UBRR = f(osc)/(16*BAUD)

	UCSR1A = 0;
	UCSR1B = (1 << TXEN1);
	UCSR1C = (3<<UCSZ10);	// 1 bit STOP
	// UCSZ0 : 8 bits transmis
	// Aucune Parite: UPM1=0 et UPM0 = 0
}

/*******************************************************************/


/**************				**************/

void uart1_switch_mode_int(void)
{
	UCSR1B |= (1 << TXCIE1) | (1 << RXCIE1);
}

/*****************************************/


/**************				**************/

int uart1_putchar(char car, FILE *stream)// fonction envoyer caractere
{
	while(!(UCSR1A & (1 << UDRE1)));// attendre fin d'emission du caractere precedent

	UDR1 = car;//envoyer caractere

	return 0;
}

/*****************************************/


/**************				**************/

void uart1_send_char_fast(char c)
{
	UDR1 = c;
}

/*****************************************/

/**************				**************/

unsigned char uart1_getchar(void)	// fonction attendre reception
{
	while(!(UCSR1A & (1 << RXC1))); // attendre la reception
	
	return UDR1;
}

/*****************************************/
